require "ruby-progressbar"
require "csv"
require "ostruct"

module EcbExchange
  class Import
    def initialize(downloader:, storage: Storage.new)
      @downloader = downloader
      @storage = storage
    end

    def call
      valid_imports = if $stdout.isatty && !$RSpec
                        with_progressbar(csv_data.size) { |bar| import_csv(bar) }
                      else
                        import_csv
                      end

      OpenStruct.new(size: valid_imports)
    end

    private

    attr_reader :downloader, :storage

    def with_progressbar(total)
      progressbar = ProgressBar.create(total: total)

      yield progressbar
    end

    def csv_data
      @csv_data ||= CSV.parse(downloader.fetch)
    end

    def import_csv(progressbar = nil)
      valid_imports = 0

      csv_data.each do |csv_row|
        row = Row.new(*csv_row)

        if row.valid?
          storage.update(date: row.date, value: row.exchange)
          valid_imports += 1
        end

        progressbar.increment if progressbar

      end

      valid_imports
    end
  end
end

require "ecb_exchange/import/row"
