module EcbExchange
  class Config
    attr_writer :database, :namespace
    attr_reader :database

    def initialize(&block)
      block.call(self) if block_given?
    end

    def namespace
      @namespace ||= "ecb_exchange"
    end
  end
end
