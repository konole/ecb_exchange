require "faraday"

module EcbExchange
  class Download
    def initialize(url:)
      @url = url
    end

    def fetch
      client.get.body
    end

    private

    attr_reader :url

    def client
      @client ||= Faraday.new(url)
    end
  end
end
