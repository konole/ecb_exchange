module EcbExchange
  class Import
    class Row < Struct.new(:date, :exchange)
      def valid?
        date_valid? && exchange_valid?
      end

      def date
        super.to_s.strip
      end

      def exchange
        super.to_s.strip
      end

      def date_valid?
        date && Date.parse(date) && true
      rescue ArgumentError
        false
      end

      def exchange_valid?
        Float(exchange) && true
      rescue ArgumentError, TypeError
        false
      end
    end
  end
end
