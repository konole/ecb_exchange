module EcbExchange
  class Storage
    def initialize(database: nil, namespace: nil)
      @database = database || EcbExchange.config.database
      @namespace = namespace
    end

    def update(date:, value:)
      database.set(key(date), value)
    end

    def fetch(date:)
      database.get(key(date))
    end

    def size
      database.size
    end

    private

    attr_reader :database

    def key(date)
      [namespace, date].join(":")
    end

    def namespace
      @namespace || EcbExchange.config.namespace
    end
  end
end
