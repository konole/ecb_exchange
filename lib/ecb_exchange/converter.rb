require 'bigdecimal'

module EcbExchange
  class Converter
    class Rate
      attr_reader :date, :value

      def initialize(value:, date:)
        @value = value.to_f
        @date = date
      end
    end
    ExchangeNotFound = Class.new(StandardError)

    def self.convert(value, date)
      new.call(value: value, date: Date.parse(date))
    end

    def initialize(storage: Storage.new)
      @storage = storage
    end

    def call(value:, date:, retries: 5)
      value = BigDecimal(value)
      rate = storage.fetch(date: date.to_s)

      raise ExchangeNotFound if rate.nil? && retries.zero?

      return call(value: value, date: date.prev_day, retries: retries-1) if rate.nil?

      Rate.new(value: value / BigDecimal(rate), date: date)
    end

    private

    attr_reader :storage
  end
end
