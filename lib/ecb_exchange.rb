require "ecb_exchange/version"
require "ecb_exchange/config"
require "ecb_exchange/converter"
require "ecb_exchange/storage"

module EcbExchange
  class << self
    def config
      @config ||= Config.new
    end

    def setup(&block)
      @config = Config.new(&block)
    end

    def rate_for(value:, date:)
      Converter.convert(value, date)
    end
  end
end
