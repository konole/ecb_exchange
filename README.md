# Requirements

Redis

## Installation

```ruby
gem 'ecb_exchange'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ecb_exchange

## Usage

1. Preload rates in Redis
```sh
bin/rake download
```

2. Run console
```sh
bin/console
```

3. Fetch rates for date

```ruby
[1] pry(main)> rate = EcbExchange.rate_for(value: 100, date: '2000-01-03')
=> #<EcbExchange::Converter::Rate:0x007fb60487c230 @date=#<Date: 2000-01-03 ((2451547j,0s,0n),+0s,2299161j)>, @value=99.10802775024777>
[2] pry(main)> rate.value
=> 99.10802775024777
[3] pry(main)> rate.date
=> #<Date: 2000-01-03 ((2451547j,0s,0n),+0s,2299161j)>
[4] pry(main)> rate = EcbExchange.rate_for(value: 100, date: '2000-01-01')
=> #<EcbExchange::Converter::Rate:0x007fb604133bb8 @date=#<Date: 1999-12-30 ((2451543j,0s,0n),+0s,2299161j)>, @value=99.54210631096954>
[5] pry(main)> rate.value
=> 99.54210631096954
[6] pry(main)> rate.date
=> #<Date: 1999-12-30 ((2451543j,0s,0n),+0s,2299161j)>
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
