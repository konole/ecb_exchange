$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
$RSpec=true

require 'ecb_exchange'

class MemoryDatabase
  def initialize
    @exchanges = {}
  end

  def set(key, value)
    @exchanges[key] = value.to_s
  end

  def get(key)
    @exchanges[key]
  end

  def size
    @exchanges.size
  end
end
