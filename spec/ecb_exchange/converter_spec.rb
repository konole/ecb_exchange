require "spec_helper"

RSpec.describe EcbExchange::Converter do
  let(:db) { MemoryDatabase.new }
  let(:storage) { EcbExchange::Storage.new(database: db, namespace: "foo") }

  subject(:converter) { described_class.new(storage: storage) }

  before do
    db.set('foo:2015-10-10', 2.0)
    db.set('foo:2015-10-08', 1.0)
  end

  let(:exchange) { subject.call(value: 100, date: date) }

  describe "currency exchange" do
    context "when exchange exists" do
      let(:date) { Date.parse('2015-10-10') }
      it "returns exchanged currency value for given date" do

        aggregate_failures do
          expect(exchange.value).to eq(50)
          expect(exchange.date).to eq(date)
        end
      end
    end

    context "when exchange does not exist" do
      let(:date) { Date.parse('2015-01-01') }

      it "raises error" do
        expect {
          exchange
        }.to raise_error(EcbExchange::Converter::ExchangeNotFound)
      end
    end

    describe "previous days lookup" do
      context "when exchange exists for previous day " do
        let(:date) { Date.parse('2015-10-11') }

        it "returns it" do
          aggregate_failures do
            expect(exchange.value).to eq(50)
            expect(exchange.date).to eq(Date.parse('2015-10-10'))
          end
        end
      end

      context "when the latest exchange exists 5 days in past" do
        let(:date) { Date.parse('2015-10-15') }

        it "returns it" do
          aggregate_failures do
            expect(exchange.value).to eq(50)
            expect(exchange.date).to eq(Date.parse('2015-10-10'))
          end
        end
      end

      context "when the latest exchange exists 6 days in past" do
        let(:date) { Date.parse('2015-10-16') }

        it "raises error" do
          expect {
            exchange
          }.to raise_error(EcbExchange::Converter::ExchangeNotFound)
        end
      end
    end
  end
end
