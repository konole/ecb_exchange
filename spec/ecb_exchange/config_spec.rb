require "spec_helper"

RSpec.describe EcbExchange::Config do
  let(:database) { double }

  it "allows to setup database during initialization" do
    config = described_class.new do |c|
      c.database = database
    end

    expect(config.database).to eq(database)
  end

  it "has default namespace" do
    expect(described_class.new.namespace).to eq("ecb_exchange")
  end
end
