require "spec_helper"
require "ecb_exchange/import"
require "ecb_exchange/storage"

describe EcbExchange::Import do
  let(:response) do
    <<-CSV
 Data Source in SDW: null
 ,EXR.D.USD.EUR.SP00.A
 ,"ECB reference exchange rate, US dollar/Euro, 2:15 pm (C.E.T.)"
 Collection:,Average of observations through period (A)
 Period\Unit:,[US dollar ]
 2016-02-12,1.1275
 2016-02-11,1.1347
 Wrong line
 2014-02-11,1.0000
    CSV
  end

  let(:storage) { EcbExchange::Storage.new(database: MemoryDatabase.new) }
  let(:downloader) { double(fetch: response) }

  subject(:import) { described_class.new(downloader: downloader, storage: storage) }

  describe "#call" do
    it "imports valid rates to database" do
      result = import.call

      aggregate_failures do
        expect(storage.size).to eq(3)
        expect(storage.fetch(date: '2016-02-12')).to eq('1.1275')
        expect(storage.fetch(date: '2016-02-11')).to eq('1.1347')
        expect(storage.fetch(date:'2014-02-11')).to eq('1.0000')

        expect(result.size).to eq(3)
      end
    end

    context "when exchange rate already exists in database" do
      before do
        storage.update(date: '2016-04-12', value: '2.0000')
        storage.update(date: '2016-02-12', value: '2.0000')
      end

      it "overrides current value with new one" do
        result = import.call

        aggregate_failures do
          expect(storage.fetch(date:'2016-04-12')).to eq('2.0000')
          expect(storage.fetch(date:'2016-02-12')).to eq('1.1275')
          expect(storage.fetch(date:'2016-02-11')).to eq('1.1347')
          expect(storage.fetch(date:'2014-02-11')).to eq('1.0000')
          expect(storage.size).to eq(4)
          expect(result.size).to eq(3)
        end
      end
    end
  end
end
