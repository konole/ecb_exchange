require "spec_helper"

RSpec.describe EcbExchange::Storage do
  before do
    EcbExchange.setup do |c|
      c.namespace = "foo"
    end
  end

  let(:database) { double }

  subject(:storage) do
    described_class.new(database: database)
  end

  describe "#fetch" do
    it "proxies the call to database#get with proper namespace" do
      expect(database).to receive(:get).with("foo:2000")

      storage.fetch(date: "2000")
    end
  end
end
